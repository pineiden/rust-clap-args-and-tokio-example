Clap Test
========

Is a functional example that tests how to use 'clap' crate combined
with async functions. 

The async function uses tokio and futures. Raise inside two async
loops and works concurrently. 

Is a working and basic example combining some kind of interface layer.

A second level of implementation
================================

A corroutine generate an object that will be sended by a 'broadcast
channel' and on the other corroutine the value is received as is
created. (If send a String then receive a String). Internally is not
necessary to serialize the object to send to the other function.
