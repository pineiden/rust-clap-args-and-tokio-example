#[macro_use]
extern crate clap;
use clap::{Arg, App};
mod functions;
use tracing::{event, span, Level, Instrument};
// tracing subscriptions 

// use tracing_bunyan_formatter::{BunyanFormattingLayer, JsonStorageLayer};
// use tracing_subscriber::Registry;
// use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber;

pub fn isnumeric(val: String)-> Result<(), String> {
	match val.parse::<u16>() {
		Ok(n)  => {
			if 1<=n && n<=u16::MAX {
				return Ok(());
			};
			Err("Value must be from 1 to 65635".to_string())
		},
		Err(_e) => Err("Must be numeric value".to_string())
	}
}

#[tokio::main]
pub async fn main() {
    // let formatting_layer = BunyanFormattingLayer::new(
	// 	"tracing test".into(), 
	// 	std::io::stdout);
    // let subscriber = Registry::default()
    //     .with(JsonStorageLayer)
    //     .with(formatting_layer);
    // tracing::subscriber::set_global_default(subscriber).unwrap();

    tracing_subscriber::fmt::init();

	let span_main = span!(Level::TRACE, "main");
	println!("Iniciando servidor datagen");
	// let ports = (1..65536)
	// 	.map(|value| {
	// 		value.to_string()
	// 	} )
	// 	.collect::<Vec<String>>();
	// let posible_ports = ports
	// 	.iter()
	// 	.map(|s| s.as_str())
	// 	.collect::<Vec<&str>>();
	// obtener parámetros de la línea de comandos
	let matches = App::new("Datagen Server CLI")
		.author(crate_authors!())
		.version(crate_version!())
		.about("This is the datagen server cli app")
		.arg(Arg::with_name("scheme")
			 .short("s")
			 .long("scheme")
			 .takes_value(true)
			 .value_name("SCHEME")
			 .default_value("http")
			 .possible_value("http")
			 .possible_value("https")
			 .case_insensitive(true)
			 .help("Give a scheme: http/https"))
		.arg(Arg::with_name("host")			 
			 .short("h")
			 .long("host")
			 .takes_value(true)
			 .value_name("HOST")
			 .default_value("0.0.0.0")
			 .case_insensitive(true)
			 .help("Give a hostname, url or ip"))
		.arg(Arg::with_name("port")
			 .short("p")
			 .long("port")
			 .takes_value(true)
			 .value_name("NAME")
			 .default_value("80")
			 .help("Give a name for protocol"))
		.arg(Arg::with_name("protocol")
			 .short("r")
			 .long("protocol")
			 .takes_value(true)
			 .value_name("VERSION")
			 .default_value("TEST")
			 .case_insensitive(true)
			 .help("Give a version for communitation protocol"))
		.arg(Arg::with_name("protocol-version")
			 .short("v")
			 .long("protocol-version")
			 .takes_value(true)
			 .value_name("VERSION")
			 .default_value("dg-2021")
			 .case_insensitive(true)
			 .help("Give a version for communitation protocol"))
		.arg(Arg::with_name("equipment-name")
			 .short("e")
			 .long("equipment-name")
			 .takes_value(true)
			 .value_name("NAME")
			 .default_value("TEST")
			 .case_insensitive(true)
			 .help("Give an human name for this equipment"))
		.arg(Arg::with_name("equipment-serie")
			 .short("n")
			 .long("equipment-serie")
			 .takes_value(true)
			 .default_value("1")
			 .value_name("NUMBER")
			 .validator(isnumeric)
			 .help("Give a serie number for this equipment"))
		.arg(Arg::with_name("end-flag")
			 .short("f")
			 .long("end-flag")
			 .takes_value(true)
			 .value_name("SEPARATOR")
			 .default_value("*end*")
			 .case_insensitive(true)
			 .help("Give an end flag string"))
		.get_matches();

	if let Some(scheme) = matches.value_of("scheme") {
		println!("Scheme {}", scheme);
	}
	let scheme = matches.value_of("scheme").unwrap().to_string();

	if let Some(host) = matches.value_of("host") {
		println!("host {}", host);
	}
	let host = matches.value_of("host").unwrap().to_string();


	if let Some(port) = matches.value_of("port") {
		println!("port {}", port);
	}
	let port = matches.value_of("port")
		.unwrap()
		.to_string()
		.parse::<u16>()
		.unwrap();

	if let Some(end_flag) = matches.value_of("end-flag") {
		println!("end-flag {}", end_flag);
	}

	let end_flag = matches.value_of("end-flag").unwrap().to_string();


	if let Some(protocol) = matches.value_of("protocol") {
		println!("protocol version {}", protocol);
	}
	let protocol = matches.value_of("protocol").unwrap().to_string();


	if let Some(version) = matches.value_of("protocol-version") {
		println!("protocol version {}", version);
	}
	let version = matches.value_of("protocol-version").unwrap().to_string();
	
	if let Some(name) = matches.value_of("equipment-name") {
		println!("equipment name {}", name);
	}
	let name = matches.value_of("equipment-name").unwrap().to_string();

	if let Some(serie) = matches.value_of("equipment-serie") {
		println!("equipment serie {}", serie);
	}

	let serie = matches.value_of("equipment-serie")
		.unwrap()
		.to_string()
		.parse::<u16>()
		.unwrap();

	let _ =  span_main.in_scope(||{

	});

	functions::datagen_server(
		&scheme,
		&host,
		&port,
		&protocol,
		&version, 
		&name, 
		&serie, 
		&end_flag)
		.instrument(span_main)
		.await;

}

