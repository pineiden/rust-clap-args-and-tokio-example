use tokio::time::{sleep, Duration};
use tokio::{self};
use chrono::{Utc};
use futures::future::{self};
use tokio::sync::broadcast;
use rand::seq::SliceRandom;
use tokio::sync::broadcast::{Sender};
use tracing::{event, span, Level, Instrument};
use tracing::info;

#[tracing::instrument(level = "info")]
async fn data(tx_source:Sender<String>) {
	// create a list to select randommly from this and send.
	let names: Vec<String> = vec![
		"Torcuato".to_string(),
		"Romeo".to_string(),
		"Julieta".to_string(),
		"Miguel".to_string(), 
		"Frodo".to_string(),
		"Helena".to_string()];

	info!("Inside data generator corroutine");

	loop{
		   let now = Utc::now();
		   let name = names.choose(&mut rand::thread_rng()).unwrap();
		   tx_source.send(name.clone()).unwrap();
		   info!("DATA | {} |  function executed | sended {}", now, name);
		   sleep(Duration::from_secs(10)).await;
		}
}

#[tracing::instrument(level = "info")]
async fn serve(tx_source:Sender<String>) {
	let mut rx = tx_source.subscribe();
	info!("Inside serve corroutine");
	loop {
		    let result = rx.recv().await.unwrap();
			let now = Utc::now();
			info!("SERVE | {} | function executed | received {}", now, result);
			sleep(Duration::from_secs(8)).await;
		}
}


/*
This function tend to understand how operate in general, withouth
calling the real functions, create two task to run asyncronously

*/
#[tracing::instrument(level = "info")]
pub async fn datagen_server(
	_scheme: &String,
	_host: &String,
	_port: &u16,
	_protocol: &String, 
	_version: &String,
	_equipment_name: &String,
	_equipment_serie: &u16,
	_end_flag: &String
) {

    let (tx_source, _rx) = broadcast::channel(100);
	let span_data = span!(Level::TRACE, "DATA");
	let span_serve = span!(Level::TRACE, "SERVE");

	info!("Starting datagen_server");

	let gen_tx_source = tx_source.clone();

	let data_task = tokio::spawn(async move {
		data(gen_tx_source)
			.instrument(span_data)
			.await;
	});

	let server_tx_source = tx_source.clone();

	let serve_task = tokio::spawn(async move {
		serve(server_tx_source)
			.instrument(span_serve)
			.await;
	});

	let v = vec![data_task, serve_task];

	let _outputs = future::try_join_all(v).await;

}
